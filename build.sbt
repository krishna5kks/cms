
name := "cms"
 
version := "1.0" 
      
lazy val `cms` = (project in file(".")).enablePlugins(PlayScala)

def sysPropOrDefault(propName: String, default: String): String = Option(System.getProperty(propName)).getOrElse(default)
val  number = sysPropOrDefault("versionnum", "latest")

def sysrepoOrDefault(repoName: String, default: String): String = Option(System.getProperty(repoName)).getOrElse(default)
val repo = sysPropOrDefault("reponame", "sorry")

lazy val version_num = (project in file(".")).enablePlugins(JavaServerAppPackaging)
  .settings(
    javaOptions in Universal ++= Seq(
      s"-Dhttp.versionnum=$number"
    )
  )
lazy val reposgitry = (project in file(".")).enablePlugins(JavaServerAppPackaging)
  .settings(
    javaOptions in Universal ++= Seq(
      s"-Dhttp.reponame=$repo"
    )
  )


lazy val root = (project in file(".")).enablePlugins(PlayScala)



// Docker Settings
enablePlugins(sbtdocker.DockerPlugin)
dockerAutoPackageJavaApplication()

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
      
scalaVersion := "2.12.2"

libraryDependencies ++= Seq( jdbc , ehcache , ws , specs2 % Test , guice )
libraryDependencies += "org.apache.solr" % "solr-solrj" % "7.2.0"
libraryDependencies += "net.liftweb" %% "lift-json" % "3.2.0-M3"
libraryDependencies += "org.postgresql" % "postgresql" % "9.4.1212"
libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.16"

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

dockerfile in docker := {
  val jarFile = Keys.`package`.in(Compile, packageBin).value
  val classpath = (managedClasspath in Compile).value
  val mainclass = mainClass.in(Compile, packageBin).value.get
  val libs = "/app/libs"
  val jarTarget = "/app/" + jarFile.name

  new Dockerfile {
    // Use a base image that contain Java
    from("vikaschenny/java:1.8")
    // Expose port 9000
    expose(9000)

    // Copy all dependencies to 'libs' in the staging directory
    classpath.files.foreach { depFile =>
      val target = file(libs) / depFile.name
      stageFile(depFile, target)
    }
    // Add the libs dir from the
    addRaw(libs, libs)
    // Add the generated jar file
    add(jarFile, jarTarget)
    // The classpath is the 'libs' dir and the produced jar file
    val classpathString = s"$libs/*:$jarTarget"
    // Set the entry point to start the application using the main class
    cmd("java", "-cp", classpathString, mainclass)
  }
}

imageNames in docker := {
  val imageName = ImageName(
    namespace = Some(repo),
    repository = name.value,
    tag = Some(number))
  Seq(imageName)
}

      