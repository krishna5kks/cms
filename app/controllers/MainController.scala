package controllers

import entities.SkuAttributes
import javax.inject.Inject
import net.liftweb.json.{JsonParser, NoTypeHints, Serialization}
import play.api.Logger
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import services.{CmsUtility, UUIDSerializerDeserializer}

class MainController  @Inject()(cc: ControllerComponents) extends AbstractController(cc)  {
  implicit var formats = Serialization.formats(NoTypeHints)+ new UUIDSerializerDeserializer
  val logger= Logger(this.getClass)


  def getSkus(pageNo:String,rows:String) = Action{
    val skuListJson = Serialization.write(CmsUtility.getSku(pageNo,rows))
Ok(skuListJson)
  }

  def updateSku() = Action{
    request =>
      logger.info("************** requestReceived  **************" + request.body);
      val skuAttributes = CmsUtility.extractSkuAttributesfromRequest(request)
     val response = CmsUtility.updateSku(skuAttributes)
      logger.info(response.toString)
    Ok("updated successfully")
  }

  def addSku() = Action{
    request =>
      logger.info("************** requestReceived  **************" + request.body);
      val skuAttributes = CmsUtility.extractSkuAttributesfromRequest(request)
      val response = CmsUtility.addSku(skuAttributes)
    Ok("added successfully")
  }

}
