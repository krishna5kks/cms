package entities

case class SkuAttributes(var skuid : String= "",
                          var PartNumber: String = "",
                         var Brand: String = "",
                         var Condition: String = "",
                         var CategoryName: String = "",
                         var Description: String = "",
                         var HazardousMaterialCode: String = "",
                         var Height: String = "",
                         var ImageURL: String = "",
                         var ItemLevelGTIN: String = "",
                         var KeyID: String = "",
                         var Length: String = "",
                         var MinimumOrderQuantity: String = "",
                         var Name: String = "",
                         var Note: String = "",
                         var Position: String = "",
                         var ProductAttribute: String = "",
                         var ProductType: String = "",
                         var Price_Primary: String = "",
                         var QuantityPerApplication: String = "",
                         var QuantityofEaches: String = "",
                         var SubCategoryName: String = "",
                         var Title: String = "",
                         var Weight: String = "",
                         var BrandType: String = "",
                         var Width: String = "",
                         var distributorid: String = "",
                         var isfreight: String = "",
                         var shipping_method: String = "",
                         var partinterchange: String = "",
                         var packageList: String = "",
                         var kitParts: String = "",
                         var parentKitId: String = "") {

  override def equals(skuAttributes: Any): Boolean = {
    skuAttributes match {
      case sku: SkuAttributes => {
        this.Price_Primary == sku.Price_Primary &&
          this.Brand == sku.Brand &&
          this.Position == sku.Position
      }
      case _ => false
    }
  }

  override def hashCode(): Int = (Price_Primary + Position + Brand).hashCode
}
