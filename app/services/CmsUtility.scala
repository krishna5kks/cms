package services

import java.net.URLEncoder

import com.typesafe.config.ConfigFactory
import entities.SkuAttributes
import org.apache.solr.client.solrj.SolrQuery
import org.apache.solr.client.solrj.SolrQuery.ORDER
import org.apache.solr.client.solrj.impl.HttpSolrClient
import org.apache.solr.client.solrj.request.{AbstractUpdateRequest, UpdateRequest}
import org.apache.solr.client.solrj.response.{QueryResponse, TermsResponse, UpdateResponse}
import org.apache.solr.common.{SolrDocument, SolrDocumentList, SolrInputDocument}
import play.api.Logger
import java.util
import java.sql.{Connection, DriverManager, ResultSet}
import java.text.SimpleDateFormat
import java.util.Date

import net.liftweb.json.{JsonParser, NoTypeHints, Serialization}
import play.api.mvc.{AnyContent, Request}

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object CmsUtility {
  implicit var formats = Serialization.formats(NoTypeHints)+ new UUIDSerializerDeserializer
private val config = ConfigFactory.load
  val logger = Logger(this.getClass)

  def client: HttpSolrClient = new HttpSolrClient.Builder(config.getString("solr.solrurl")).build()

def getSku(pageNo: String,rows: String):List[SkuAttributes]={
val query = new SolrQuery()
  .setQuery("*:*")
  .setStart((pageNo.toInt - 1) * rows.toInt)
  .setRows(rows.toInt)
  .addSort("score", ORDER.desc)
  logger.info("Query"+query)
  val response = client.query(query)
val result = solrResultFetch(response)
  result
}

  def solrResultFetch(response: QueryResponse): List[SkuAttributes] = {
    // val skuListSet = processResults(response.getResults)
    val skuListSet = processResults(response.getResults)
    if (skuListSet.nonEmpty) {
      skuListSet
    } else {
      List(SkuAttributes())
    }
  }
  def processResults(results: SolrDocumentList): List[SkuAttributes] = {
    if (results.isEmpty)
      return List.empty[SkuAttributes]

    val skuListSet = results.map(doc => getSkuInfo(doc)).toList

    val searchResult = skuListSet //adding instock + out of stock
    //    logger.info("Price Sku Found are  " + skuListSet)

    //removing items if price info is not available
    searchResult
  }

  def getSkuInfo(doc: SolrDocument): SkuAttributes = {
    val skuInfoNode = SkuAttributes()
    skuInfoNode.Condition = if (!getValues("Condition", doc).isEmpty && !getValues("Condition", doc).equals(" ")) getValues("Condition", doc)
      else "New"
    skuInfoNode.Brand = if (!getValues("Brand", doc).isEmpty) getValues("Brand", doc) else "&"
    skuInfoNode.Price_Primary = getValues("Price_Primary", doc)
    skuInfoNode.PartNumber = getValues("PartNumber", doc)
    skuInfoNode.Description = getValues("Description", doc)
    //skuInfoNode.oepartNumber = getValues("OEPartNumber", doc)
    skuInfoNode.Position = getValues("Position", doc)
    skuInfoNode.Length = (Math.round(getDimensionValue("Length", doc).toDouble * 100.0) / 100.0).toString
    skuInfoNode.Height = (Math.round(getDimensionValue("Height", doc).toDouble * 100.0) / 100.0).toString
    skuInfoNode.ItemLevelGTIN = getValues("ItemLevelGTIN", doc)
    skuInfoNode.Width = (Math.round(getDimensionValue("Width", doc).toDouble * 100.0) / 100.0).toString
    skuInfoNode.Weight = if (getDimensionValue("Weight", doc).toDouble >= 1.00)
        (Math.round(getDimensionValue("Weight", doc).toDouble * 100.0) / 100.0).toString
      else
        (Math.round(getDimensionValue("Weight", doc).toDouble * 16 * 100.0) / 100.0).toString
    skuInfoNode.CategoryName = getValues("CategoryName", doc)
    skuInfoNode.SubCategoryName = getValues("SubCategoryName", doc)
    skuInfoNode.Title = getValues("Title", doc)
    skuInfoNode.HazardousMaterialCode = getValues("HazardousMaterialCode", doc)
    skuInfoNode.MinimumOrderQuantity = getValues("MinimumOrderQuantity", doc)
    skuInfoNode.QuantityPerApplication = getValues("QuantityPerApplication", doc)
    skuInfoNode.QuantityofEaches = getValues("QuantityofEaches", doc)
    skuInfoNode.ProductAttribute = getValues("ProductAttribute", doc).replace("{{", "[{").replace("}}", "}]")
    skuInfoNode.ImageURL = getValues("ImageURL",doc)
    skuInfoNode.distributorid = getValues("distributorid", doc)
    skuInfoNode.isfreight = getValues("isfreight", doc)
    skuInfoNode.skuid = getValues("skuid", doc)
    skuInfoNode.KeyID = getValues("KeyID", doc)
    skuInfoNode.Name = getValues("Name", doc)
    skuInfoNode.Note = getValues("Note", doc)
    skuInfoNode.ProductType = getValues("ProductType", doc)
    skuInfoNode.shipping_method = getValues("shipping_method", doc)
    skuInfoNode.BrandType = getValues("BrandType", doc)
    skuInfoNode.packageList = getValues("Package",doc)
    skuInfoNode.partinterchange = getValues("partinterchange",doc)
    skuInfoNode.parentKitId = getValues("ParentKitId",doc)
    skuInfoNode.kitParts = getValues("KitParts",doc)
    skuInfoNode
  }

  def getValues(value: String, doc: SolrDocument): String = {
    if (doc.contains(value)) doc.getFieldValue(value).toString else ""
  }

  def getDimensionValue(value: String, doc: SolrDocument): String = {
    val attValue = doc.getFieldValue(value)
    if (attValue != null && !attValue.toString.isEmpty) attValue.toString
    else "0.0"
  }
//def updateSku(skuid: Option[String],partNumber: Option[String],title: Option[String],description: Option[String],imageURL: Option[String],category: Option[String],subcategory: Option[String],DimensionUOM: Option[String],WeightUOM: Option[String],Length: Option[String],Height: Option[String],ItemLevelGTIN: Option[String],Width: Option[String],Weight: Option[String],imageList: Option[String],brand: Option[String],product_url: Option[String],position: Option[String],condition: Option[String],pricePrimary: Option[String],distributor_group: Option[String],hazardousMaterialCode: Option[String],availableQuantity: Option[String],minimumOrderQunatity: Option[String],quantityPerApplication: Option[String],quantityofEaches: Option[String],productAttribute: Option[String],isFreight: Option[String]):Any={
def updateSku(sku:SkuAttributes):Any={

  var updateRequest: UpdateRequest = new UpdateRequest();
  updateRequest.setAction( AbstractUpdateRequest.ACTION.COMMIT, false, false);

  updateRequest = updateRequest.add(solrDoc(sku))
  var updateResponse:UpdateResponse = updateRequest.process(client)
  updateResponse
}

  def solrDoc(skuAttributes:SkuAttributes):SolrInputDocument={
    var doc = new SolrInputDocument()
    var updateStmtStr:String=""
    var addStmtStr:String=""
    var addNameStmtStr:String=""

    doc.addField("skuid", skuAttributes.skuid)
    if(!skuAttributes.distributorid.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.distributorid)
      doc.addField("distributorid", value)
      updateStmtStr +=  " distributorid='"+skuAttributes.distributorid+"',"
      addStmtStr += "'"+skuAttributes.distributorid+"',"
      addNameStmtStr += "distributorid,"
    }
    if(!skuAttributes.PartNumber.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.PartNumber)
      doc.addField("PartNumber", value)
      updateStmtStr +=  " PartNumber='"+ skuAttributes.PartNumber +"',"
      addStmtStr += "'"+skuAttributes.PartNumber+"',"
      addNameStmtStr += "PartNumber,"
    }
    if(!skuAttributes.SubCategoryName.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.SubCategoryName)
      doc.addField("SubCategoryName", value)
      updateStmtStr +=  " SubCategoryName='"+ skuAttributes.SubCategoryName +"',"
      addStmtStr += "'"+skuAttributes.SubCategoryName+"',"
      addNameStmtStr += "SubCategoryName,"
    }
    if(!skuAttributes.CategoryName.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.CategoryName)
      doc.addField("CategoryName", value)
      updateStmtStr +=  " CategoryName='"+ skuAttributes.CategoryName +"',"
      addStmtStr += "'"+skuAttributes.CategoryName+"',"
      addNameStmtStr += "CategoryName,"
    }
    if(!skuAttributes.Price_Primary.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.Price_Primary)
      doc.addField("Price_Primary", value)
      updateStmtStr +=  " Price_Primary='"+ skuAttributes.Price_Primary +"',"
      addStmtStr += "'"+skuAttributes.Price_Primary+"',"
      addNameStmtStr += "Price_Primary,"
    }
    if(!skuAttributes.Brand.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.Brand)
      doc.addField("Brand", value)
      updateStmtStr +=  " Brand='"+ skuAttributes.Brand +"',"
      addStmtStr += "'"+skuAttributes.Brand+"',"
      addNameStmtStr += "Brand,"
    }
    if(!skuAttributes.BrandType.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.BrandType)
      doc.addField("BrandType", value)
      updateStmtStr +=  " BrandType='"+ skuAttributes.BrandType +"',"
      addStmtStr += "'"+skuAttributes.BrandType+"',"
      addNameStmtStr += "BrandType,"
    }
    if(!skuAttributes.Condition.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.Condition)
      doc.addField("Condition", value)
      updateStmtStr +=  " Condition='"+ skuAttributes.Condition +"',"
      addStmtStr += "'"+skuAttributes.Condition+"',"
      addNameStmtStr += "Condition,"
    }
    if(!skuAttributes.Description.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.Description)
      doc.addField("Description", value)
      updateStmtStr +=  " Description='"+ skuAttributes.Description +"',"
      addStmtStr += "'"+skuAttributes.Description+"',"
      addNameStmtStr += "Description,"
    }
    if(!skuAttributes.HazardousMaterialCode.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.HazardousMaterialCode)
      doc.addField("HazardousMaterialCode", value)
      updateStmtStr +=  " HazardousMaterialCode='"+ skuAttributes.HazardousMaterialCode +"',"
      addStmtStr += "'"+skuAttributes.HazardousMaterialCode+"',"
      addNameStmtStr += "HazardousMaterialCode,"
    }
    if(!skuAttributes.Height.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.Height)
      doc.addField("Height", value)
      updateStmtStr +=  " Height='"+ skuAttributes.Height +"',"
      addStmtStr += "'"+skuAttributes.Height+"',"
      addNameStmtStr += "Height,"
    }
    if(!skuAttributes.ImageURL.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.ImageURL)
      doc.addField("ImageURL", value)
      updateStmtStr +=  " ImageURL='"+ skuAttributes.ImageURL +"',"
      addStmtStr += "'"+skuAttributes.ImageURL+"',"
      addNameStmtStr += "ImageURL,"
    }
    if(!skuAttributes.ItemLevelGTIN.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.ItemLevelGTIN)
      doc.addField("ItemLevelGTIN", value)
      updateStmtStr +=  " ItemLevelGTIN='"+ skuAttributes.ItemLevelGTIN +"',"
      addStmtStr += "'"+skuAttributes.ItemLevelGTIN+"',"
      addNameStmtStr += "ItemLevelGTIN,"
    }
    if(!skuAttributes.isfreight.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.isfreight)
      doc.addField("isfreight", value)
      updateStmtStr +=  " isfreight='"+ skuAttributes.isfreight +"',"
      addStmtStr += "'"+skuAttributes.isfreight+"',"
      addNameStmtStr += "isfreight,"
    }
    if(!skuAttributes.KeyID.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.KeyID)
      doc.addField("KeyID", value)
      updateStmtStr +=  " KeyID='"+ skuAttributes.KeyID +"',"
      addStmtStr += "'"+skuAttributes.KeyID+"',"
      addNameStmtStr += "KeyID,"
    }
    if(!skuAttributes.Length.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.Length)
      doc.addField("Length", value)
      updateStmtStr +=  " Length='"+ skuAttributes.Length +"',"
      addStmtStr += "'"+skuAttributes.Length+"',"
      addNameStmtStr += "Length,"
    }
    if(!skuAttributes.MinimumOrderQuantity.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.MinimumOrderQuantity)
      doc.addField("MinimumOrderQuantity", value)
      updateStmtStr +=  " MinimumOrderQuantity='"+ skuAttributes.MinimumOrderQuantity +"',"
      addStmtStr += "'"+skuAttributes.MinimumOrderQuantity+"',"
      addNameStmtStr += "MinimumOrderQuantity,"
    }
    if(!skuAttributes.Name.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.Name)
      doc.addField("Name", value)
      updateStmtStr +=  " Name='"+ skuAttributes.Name +"',"
      addStmtStr += "'"+skuAttributes.Name+"',"
      addNameStmtStr += "Name,"
    }
    if(!skuAttributes.Note.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.Note)
      doc.addField("Note", value)
      updateStmtStr +=  " Note='"+ skuAttributes.Note +"',"
      addStmtStr += "'"+skuAttributes.Note+"',"
      addNameStmtStr += "Note,"
    }
    if(!skuAttributes.Position.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.Position)
      doc.addField("Position", value)
      updateStmtStr +=  " Position='"+ skuAttributes.Position +"',"
      addStmtStr += "'"+skuAttributes.Position+"',"
      addNameStmtStr += "Position,"
    }
    if(!skuAttributes.ProductAttribute.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.ProductAttribute)
      doc.addField("ProductAttribute", value)
      updateStmtStr +=  " ProductAttribute='"+ skuAttributes.ProductAttribute.replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").replace("\\","").replace(":","/").replace("3/8'","3/8").replace(",","&") +"',"
      addStmtStr += "'"+skuAttributes.ProductAttribute.replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").replace("\\","").replace(":","/").replace("3/8'","3/8").replace(",","&")+"',"
      addNameStmtStr += "ProductAttribute,"
    }
    if(!skuAttributes.ProductType.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.ProductType)
      doc.addField("ProductType", value)
      updateStmtStr +=  " ProductType='"+ skuAttributes.ProductType +"',"
      addStmtStr += "'"+skuAttributes.ProductType+"',"
      addNameStmtStr += "ProductType,"
    }
    if(!skuAttributes.QuantityofEaches.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.QuantityofEaches)
      doc.addField("QuantityofEaches", value)
      updateStmtStr +=  " QuantityofEaches='"+ skuAttributes.QuantityofEaches +"',"
      addStmtStr += "'"+skuAttributes.QuantityofEaches+"',"
      addNameStmtStr += "QuantityofEaches,"
    }
    if(!skuAttributes.QuantityPerApplication.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.QuantityPerApplication)
      doc.addField("QuantityPerApplication", value)
      updateStmtStr +=  " QuantityPerApplication='"+ skuAttributes.QuantityPerApplication +"',"
      addStmtStr += "'"+skuAttributes.QuantityPerApplication+"',"
      addNameStmtStr += "QuantityPerApplication,"
    }
    if(!skuAttributes.shipping_method.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.shipping_method)
      doc.addField("shipping_method", value)
      updateStmtStr +=  " shipping_method='"+ skuAttributes.shipping_method +"',"
      addStmtStr += "'"+skuAttributes.shipping_method+"',"
      addNameStmtStr += "shipping_method,"
    }
    if(!skuAttributes.Title.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.Title)
      doc.addField("Title", value)
      updateStmtStr +=  " Title='"+ skuAttributes.Title +"',"
      addStmtStr += "'"+skuAttributes.Title+"',"
      addNameStmtStr += "Title,"
    }
    if(!skuAttributes.Weight.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.Weight)
      doc.addField("Weight", value)
      updateStmtStr +=  " Weight='"+ skuAttributes.Weight +"',"
      addStmtStr += "'"+skuAttributes.Weight+"',"
      addNameStmtStr += "Weight,"
    }
    if(!skuAttributes.Width.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.Width)
      doc.addField("Width", value)
      updateStmtStr +=  " Width='"+ skuAttributes.Width +"',"
      addStmtStr += "'"+skuAttributes.Width+"',"
      addNameStmtStr += "Width,"
    }
    if(!skuAttributes.packageList.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.packageList)
      doc.addField("Package", value)
    }
    if(!skuAttributes.partinterchange.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.partinterchange)
      doc.addField("partinterchange", value)
    }
    if(!skuAttributes.parentKitId.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.parentKitId)
      doc.addField("ParentKitId", value)
    }
    if(!skuAttributes.kitParts.isEmpty) {
      var value = new util.HashMap[String,String]()
      value.put("set",skuAttributes.kitParts)
      doc.addField("KitParts", value)
    }
   logger.info("skuAttributes : "+skuAttributes)
     logUpdate(skuAttributes,updateStmtStr,addStmtStr,addNameStmtStr)

    doc
  }

  def logUpdate(skuAttributes: SkuAttributes,updateStmtStr:String,addStmtStr:String,addNameStmtStr:String):Any={

    val conn_str = "jdbc:postgresql://"+config.getString("mydb.constr")+"/"+config.getString("mydb.properties.databaseName")+"?user="+config.getString("mydb.properties.user")+"&password="+config.getString("mydb.properties.password")

    val dateFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm aa")
    var submittedDateConvert = new Date()
    val updatedAt = dateFormatter.format(submittedDateConvert)

    classOf[org.postgresql.Driver]
    // Setup the connection
    val conn = DriverManager.getConnection(conn_str)
    try {
      // Configure to be Read Only
      val statement = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE)
      // Execute Query
      val existRs = statement.executeQuery("select * from sku where skuid='"+skuAttributes.skuid+"'")
      if(existRs.next()) {
        logger.info("update sku " + updateStmtStr + " update_on='" + updatedAt + "' where skuid='" + skuAttributes.skuid + "'")
        val rs = statement.executeUpdate("update sku set " + updateStmtStr + " update_on='" + updatedAt + "' where skuid='" + skuAttributes.skuid + "'")
      }else{
        logger.info("insert into sku("+addNameStmtStr+"update_on) values('" + skuAttributes.skuid + "'," + addStmtStr + "'" + updatedAt + "')")
        val rs = statement.executeUpdate("insert into sku(skuid,"+addNameStmtStr+"update_on) values('" + skuAttributes.skuid + "'," + addStmtStr + "'" + updatedAt + "')")
      }

      // Iterate Over ResultSet

    }
    finally {
      conn.close
    }
  }

  def addSku(skuAttributes: SkuAttributes):Any={
    updateSku(skuAttributes)
  }

  def extractSkuAttributesfromRequest(request: Request[AnyContent]): SkuAttributes = {
    val json = request.body.asText.get
    val jsonOb = JsonParser.parse(json)
    val skuAttributes = jsonOb.extract[SkuAttributes]
    //  logger.info("**************Message Received in request  **************" + vehicleMessage);
    skuAttributes
  }

}
