package services

import java.util.TimeZone

import org.joda.time.DateTimeZone
import play.api.Logger

class ServiceStart {
  val logger = Logger(this.getClass)

  DateTimeZone.setDefault(DateTimeZone.forID("EST5EDT"))
  TimeZone.setDefault(TimeZone.getTimeZone("EST5EDT"))
  logger.info("Application Timezone: " + TimeZone.getDefault.getDisplayName)

}

